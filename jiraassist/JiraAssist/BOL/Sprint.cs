﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BOL
{
    public class Sprint
    {
        private static List<Sprint> SprintsCache = new List<Sprint>(50);

        protected Sprint()
        {

        }

        public static Sprint CreateSprint(string textValue)
        {
            var sprintFromCache = SprintsCache.Find(spr => spr.TextValue == textValue);
            if (sprintFromCache == null)
            {
                sprintFromCache = new Sprint() { TextValue = textValue };
                SprintsCache.Add(sprintFromCache);
            }
            return sprintFromCache;
        }

        public string TextValue;

        public string name
        {
            get
            {
                var sprintParts = TextValue.Split(',');
                foreach (var part in sprintParts)
                {
                    if (part.StartsWith("name="))
                    {
                        return part.Split('=')[1];
                    }
                }
                return String.Empty;
            }
        }
        public override string ToString()
        {
            return TextValue;
        }
    }
}
