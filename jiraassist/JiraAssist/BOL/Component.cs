﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BOL
{
    [DataContract(Name = "component")]
    public class Component
    {
        [DataMember(Name = "id")]
        public int ID;
        [DataMember(Name = "name")]
        public string Name;
        [DataMember(Name = "description")]
        public string Description;


    }
}
