﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BOL
{
    [DataContract(Name = "Project")]
    public class Project
    {
        [DataMember(Name = "id")]
        public int ID;

        [DataMember(Name = "key")]
        public string pKey;

        [DataMember(Name = "name")]
        public string Name;

        [DataMember(Name = "versions")]
        public List<Version> Versions;

    }
}
