﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BOL
{
    [DataContract(Name = "person")]
    public class Person
    {
        public static Person NotAssigned = new Person() { FullName = "", ID = "", Name = "" };

        [DataMember(Name = "name")]
        public string Name;

        [DataMember( Name = "key")]
        public string ID;

        [DataMember(Name = "displayName")]
        public string FullName;

        public override string ToString()
        {
            return Name;
        }

        private static List<string> _ApplicuntUsers = null;
        public static List<string> ApplicuntUsers
        {
            get
            {
                if (_ApplicuntUsers == null)
                {
                    _ApplicuntUsers = new List<string>(20);
                    _ApplicuntUsers.AddRange("Bryan Cahow/Colin Hesketh/David Swanson/Eric Peterson/Janelle Thomas/Mike Feroah/Mike Feroah Sr/Laura Kamp/Carrie Alexander/Lana Tom".Split('/'));
                }
                return _ApplicuntUsers;
            }

        }

        public bool IsDevTeam
        {
            get
            {
                return !ApplicuntUsers.Contains(FullName);
            }
        }

        public string UniqueKey
        {
            get { return ID; }
        }


    }
}
