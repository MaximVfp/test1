﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BOL
{
    public class JiraIssueFields
    {
        public SimpleType issuetype;
        public Project project;

        //Sprint
        public List<string> customfield_10010;

        public List<Version> fixVersions;
        public List<Component> components;
        public List<string> labels;
        public string summary;
        public Person assignee;
        public string duedate;
        public string updated;
        public SimpleType status;

        //Deadline
        public string customfield_10026;
    }

    [DataContract(Name = "Simple") ]
    public class SimpleType
    {
        [DataMember(Name = "id")]
        public int ID;

        [DataMember(Name = "name")]
        public string Name;
    }
}
