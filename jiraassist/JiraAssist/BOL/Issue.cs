﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace BOL
{
    public class JiraIssuesSet
    {
        //public string expand;
        public int startAt;
        public int maxResults;
        public int total;
        public List<Issue> issues;
    }

    [DataContract(Name = "Issue")]
    public class Issue
    {
        //public string expand;
        [DataMember(Name = "id")]
        public string ID;

        [DataMember(Name = "key")]
        public string pKey;

        [DataMember(Name = "fields")]
        private JiraIssueFields Fields;

        public string ProjectKey
        {
            get
            {
                return Fields.project.pKey;
            }
        }

        public Project Project
        {
            get; set;
        }


        public SimpleType IssueType
        {
            get
            {
                return new SimpleType()
                {
                    ID = Fields.issuetype.ID,
                    Name = Fields.issuetype.Name
                };
            }
        }


        public SimpleType IssueStatus
        {
            get
            {
                return new SimpleType()
                {
                    ID = Fields.status.ID,
                    Name = Fields.status.Name
                };
            }
        }

        public void SetAssignee(Person newAssignee)
        {
            Fields.assignee = newAssignee;
        }
        public Person Assignee
        {
            get
            {
                return Fields.assignee??Person.NotAssigned;
            }
        }


        public void SetDueDate(DateTime newDueDate)
        {
            Fields.duedate = newDueDate.ToShortDateString();
        }
        public DateTime DueDate
        {
            get
            {
                return Convert.ToDateTime(Fields.duedate);
            }

        }

        public DateTime? DeadLine
        {
            get
            {
                if (Fields.customfield_10026 == null) return null;
                return Convert.ToDateTime(Fields.customfield_10026);
            }
        }

        public DateTime Updated
        {
            get
            {
                return Convert.ToDateTime(Fields.updated);
            }
        }

        public string Summary
        {
            get
            {
                return Fields.summary;
            }
            set
            {
                Fields.summary = value;
            }
        }

        public List<Component> Components
        {
            get
            {
                return Fields.components;
            }
        }

        public List<string> Labels
        {
            get
            {
                return Fields.labels;
            }
        }

        public List<Version> FixVersions
        {
            get
            {
                return Fields.fixVersions;
            }
        }

        public Sprint Sprint
        {
            get
            {
                //I expect only one sprint 
                if (Fields.customfield_10010 != null && Fields.customfield_10010.Count == 1)
                {
                    return Sprint.CreateSprint(Fields.customfield_10010[0]);
                }
                return Sprint.CreateSprint("name=n/a");
            }
        }

        private List<Worklog> _Worklogs;
        public List<Worklog> Worklogs
        {
            get
            {
                return _Worklogs;
            }
        }
        public void AddWorklogs(List<Worklog> wl)
        {
            _Worklogs = wl;
        }


    }
}
