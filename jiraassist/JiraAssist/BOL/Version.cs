﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BOL
{
    [DataContract(Name = "version")]
    public class Version
    {
        [DataMember(Name = "id")]
        public int ID;
        [DataMember(Name = "name")]
        public string Name;
        [DataMember(Name = "releaseDate")]
        public string ReleaseDate;
        [DataMember(Name = "description")]
        public string Description;
        [DataMember(Name = "projectId")]
        public int projectId;
        [DataMember(Name = "archived")]
        public bool Archived;
        [DataMember(Name = "released")]
        public bool Released;
    }
}
