﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BOL
{
    public class WorklogSet
    {
        public int startAt;
        public int maxResults;
        public int total;
        public List<Worklog> worklogs;

    }
    [DataContract(Name = "worklog")]
    public class Worklog
    {
        [DataMember(Name ="author")]
        public Person Author;
        [DataMember(Name ="timeSpent")]
        public string TimeSpent;

        [DataMember(Name = "timeSpentSeconds")]
        public int TimeSpentSeconds;

        [DataMember(Name = "started")]
        public string Started;

        [DataMember(Name = "issueId")]
        public int IssueID;

        public DateTime WorkDate
        {
            get
            {
                return Convert.ToDateTime(Started);
            }

        }

        [DataMember(Name = "id")]
        public int ID;

    }
}
