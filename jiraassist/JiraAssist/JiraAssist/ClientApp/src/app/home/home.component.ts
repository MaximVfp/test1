import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { User, Data, TokenData } from '../User';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  providers: [DataService],
  styleUrls: ['./home.style.css']
})


export class HomeComponent implements OnInit {
  private nameLog = '';
  private passLog = '';
  private logged = false;
  private dataTable = [];
  private loaded = false;
  private errorName = '';


  constructor(private dataService: DataService) { }

  ngOnInit() {
    //if (this.checkToken) {
    //  this.login();
    //}
  }

  //Логин пользователя
  login() {
    let self = this;
    this.dataService.logIn(new User(this.nameLog, this.passLog)).subscribe(function (data: TokenData) {
      localStorage.setItem('token', data.access_token);
      self.checkToken();
      self.errorName = "";
    }, err => this.errorName = 'Введите правильный логин и пароль');
  }

  //функция проверки токена
  checkToken() {
    console.log('Token check = ' + localStorage.getItem('token'));
    if (localStorage.getItem('token') != null) {
      this.logged = true;
    }
    else {
      this.logged = false;
    }
  }

  //кнопка выхода
  exit() {
    localStorage.removeItem('token');
    this.checkToken();
    this.dataTable = [];
    this.nameLog = '';
    this.passLog = '';
    this.loaded = false;
  }

  //Загрузить таблицу с сервера
  loadTable() {
    let self = this;
    this.loaded = true;
    console.log(localStorage.getItem('token'));
    this.dataService.loadTable(localStorage.getItem('token')).subscribe(function (data: TokenData) {
      self.dataTable = data.message;
      console.log(self.dataTable.length);
      for (var i = 0; i < self.dataTable.length; i++) {
        var date = new Date(self.dataTable[i].duedate);
        var deadDate = new Date(self.dataTable[i].deadLine)
        self.dataTable[i].duedate = date.toDateString();
        if (self.dataTable[i].deadLine != null) {
          self.dataTable[i].deadLine = deadDate.toDateString();
        }
      }
    });

  }

  //Скачать файл
  download() {

    this.dataService.getExcel({ token: localStorage.getItem('token') }).subscribe(function (res) {
      const data = new Blob([res], { type: 'application/vnd.ms-excel' });
      FileSaver.saveAs(data, "export.xlsx"); 
    });
  }

  //Отправить письмо
  sendEmail() {
    this.dataService.SendMessage(this.dataTable).subscribe(function (data) {
      alert("Письмо отправлено");
    });
  }

  //Врубить превью
  previewTable() {
    this.dataService.SendPreview(this.dataTable).subscribe(function (data) {
      sessionStorage.setItem('HTMLkey', data.toString());
      window.open('/preview');
      sessionStorage.removeItem('HTMLkey');
    });
  }

 }
