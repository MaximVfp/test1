import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { User } from '../User';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  providers: [DataService],
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  private errorName = '';
  private nameReg = '';
  private passReg = '';

  constructor(private dataService: DataService) { }


  registerUser() {
    this.dataService.addUser(new User(this.nameReg, this.passReg)).subscribe(function (data) {
      setTimeout(function () { window.location.replace("/") }, 1000);
    }, err => this.errorName = "Пользователь уже существует");
  }

}
