import { Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})

export class GridComponent implements OnChanges {

  @Input() tableData;

  constructor() { }

  columnDefs = [
    { headerName: 'Key', field: 'key', editable: true },
    { headerName: 'Assingnee', field: 'assignee', editable: true },
    { headerName: 'Summary', field: 'summary', cellClass: 'Myclass', editable: true, cellEditor: 'agLargeTextCellEditor' },
    { headerName: 'Duedate', field: 'duedate'},
    { headerName: 'Deadline', field: 'deadLine'}
  ];
  rowData = [];

  rowClassRules = {
  "selected-rows": function (params) {
    let date = new Date(params.data.duedate);
    let now = new Date();
    now.setHours(0, 0, 0, 0);
    return date<now;
    },
  "nowDay-rows": function (params) {
    let date = new Date(params.data.duedate);
    let now = new Date();
    now.setHours(0, 0, 0, 0);
    if (date.getTime() == now.getTime())
      return true;
    }
  };

  ngOnChanges() {
    this.rowData = this.tableData;
  }
}

