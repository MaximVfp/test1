import { Injectable,Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { User, Data, TokenData } from './User'
import { Observable } from 'rxjs/Observable';


@Injectable()
export class DataService {
  private baseUrl = '';
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.baseUrl = baseUrl;
  }

  logIn(user: User) {
    return this.http.post(this.baseUrl + 'api/account/Login', user);
    //return this.http.post(this.baseUrl + 'api/sampleData/Stub', { FirstName: 'dima', LastName: 123 });
  }
  addUser(user: User) {
    return this.http.post(this.baseUrl + 'api/account/Registration', user);
  }
  loadTable(token) {
    return this.http.post(this.baseUrl + 'api/account/GetDataTable', { filterID: '2' ,token});
  }
  SendMessage(data: Data[]) {
    return this.http.post(this.baseUrl + 'api/account/sendMessage', data );
  }
  SendPreview(data: Data[]) {
    return this.http.post(this.baseUrl + 'api/account/SendPreview', data);
  }
  getExcel(data): Observable<Blob> {
    //const headers = new HttpHeaders().set('content-type', 'multipart/form-data');
    //return this.http.post(this.baseUrl + 'api/account/GetExcel',data,{ headers, responseType: 'blob' });
    return this.http.post(this.baseUrl + 'api/account/GetExcel', data, { responseType:'blob' });
  }
}
