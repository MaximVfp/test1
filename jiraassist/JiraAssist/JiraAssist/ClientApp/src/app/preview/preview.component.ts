import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.css']
})
export class PreviewComponent implements OnInit {

  @ViewChild('dataContainer') dataContainer: ElementRef;

  constructor() { }
  private data = '';

  ngOnInit() {
    this.data = sessionStorage.getItem('HTMLkey');
    this.dataContainer.nativeElement.innerHTML = this.data;
  }
}
