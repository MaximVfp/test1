export class User {
  public Login: string;
  public Password: string;
  public Email: string;
  public SecName: string;
 
  constructor(login: string, password: string) {
    this.Login = login;
    this.Password = password;
  }
}
export class Data {
  public key: string;
  public assignee: string;
  public deadline: Date;
  public summary: string;
  public duedate: Date;
}

export class TokenData {
  public access_token: string;
  public message: Data[];
}

