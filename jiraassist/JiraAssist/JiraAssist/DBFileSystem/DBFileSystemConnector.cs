﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using JiraAssist.Models.Entities;

namespace JiraAssist.DBFileSystem
{
    public class DBFileSystemConnector : IDBFileSystem
    {
        private Dictionary<String, String> UsersData;
        private Dictionary<String, String> Tokens;

        // private String root = @"C:\Users\DmitryJ\Desktop\qwe\JiraAssist\JiraAssist\usersdata";

        private String root = $@"{Directory.GetCurrentDirectory()}\usersdata";

        public DBFileSystemConnector()
        {
            if (Directory.Exists(root))
            {
                UsersData = new Dictionary<String, String>();
                Tokens = new Dictionary<String, String>();

                foreach (var i in Directory.GetDirectories(root))
                {
                    var temp = i.Split(';');
                    var data = temp[0].Split('\\');
                    var name = temp[0].Split('\\')[data.Length - 1];
                    var pass = temp[1];

                    UsersData.Add(name, pass);
                }
            }
        }

        public bool CheckUserExistence(String log, String pass = null)
        {
            KeyValuePair<string, string> res;
            if (pass == null) res = UsersData.FirstOrDefault(x => x.Key == log);
            else res = UsersData.FirstOrDefault(x => x.Key == log && x.Value == pass);

            if (res.Key == null) return false;
            return true;
        }

        public bool AddUser(Object obj)
        {
            var user = obj as User;
            if (user == null) return false;
            if (CheckUserExistence(user.Login)) return false;

            UsersData.Add(user.Login, user.Password);

            var pathUser = $@"{root}\{user.Login};{user.Password}";
            new DirectoryInfo(pathUser).Create();

            using (var sw = new StreamWriter($@"{pathUser}\infoUser.JSON", false))
                new DataContractJsonSerializer(typeof(User)).WriteObject(sw.BaseStream, user);

            return true;
        }

        public bool RemUser(Object obj)
        {
            var user = obj as User;
            if (user == null) return false;
            if (!CheckUserExistence(user.Login)) return false;

            if (UsersData.ContainsKey(user.Login)) UsersData.Remove(user.Login);
            if (Tokens.ContainsKey(user.Login)) Tokens.Remove(user.Login);

            new DirectoryInfo($@"{root}\{user.Login};{user.Password}").Delete(true);

            return true;
        }

        public bool AddTokenToUser(Object obj, String token)
        {
            var user = obj as User;
            if (user == null) return false;
            if (!CheckUserExistence(user.Login)) return false;

            if (Tokens.ContainsKey(user.Login))
                Tokens[user.Login] = token;
            else
                Tokens.Add(user.Login, token);

            var pathUser = $@"{root}\{user.Login};{user.Password}";

            using (var sw = new StreamWriter($@"{pathUser}\tokenUser.JSON", false))
                new DataContractJsonSerializer(typeof(User)).WriteObject(sw.BaseStream, token);

            return true;
        }

        // public DellTokenToUser(..) <=> AddTokenUser(user, "toke_access: null")

        public Object GetUser(String log)
        {
            User user = null;

            if (!CheckUserExistence(log)) return user;           

            var pathUser = $@"{root}\{log};{UsersData[log]}\infoUser.JSON";

            using (var sr = new StreamReader(pathUser))
                user = new DataContractJsonSerializer(typeof(User)).ReadObject(sr.BaseStream) as User;

            return user;
        }

        public bool CheckTokenExistence(String token)
        {
            var log = Tokens.FirstOrDefault(x => x.Value == token).Key;
            return log != null;
        }

        (String log, String pass) IDBFileSystem.GetLP(String token)
        {
            var log = Tokens.FirstOrDefault(x => x.Value == token).Key;
            var pass = UsersData.FirstOrDefault(x => x.Key == log).Value;
            if (log != null && pass != null)
                return (log, pass);

            return (null, null);
        }
    }
}
