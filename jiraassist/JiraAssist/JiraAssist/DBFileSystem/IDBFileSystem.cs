﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JiraAssist.DBFileSystem
{
    public interface IDBFileSystem
    {
        bool AddUser(Object obj);

        bool RemUser(Object obj);

        bool AddTokenToUser(Object obj, String token);

        bool CheckUserExistence(String log, String pass = null);

        bool CheckTokenExistence(String token);

        Object GetUser(String log);

        (String log, String pass) GetLP(String token);
    }
}