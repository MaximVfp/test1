﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JiraAssist.Models.ViewModels.Response
{
    public class DataTableResponseViewModel
    {
        public String key { get; set; }
        public String assignee { get; set; }

        public String summary { get; set; }

        public DateTime? deadLine { get; set; }
        public DateTime? duedate { get; set; }

        public DataTableResponseViewModel() {}
    }
}
