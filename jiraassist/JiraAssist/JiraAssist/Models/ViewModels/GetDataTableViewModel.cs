﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JiraAssist.Models.ViewModels
{
    public class GetDataTableViewModel
    {
        public String token { get; set; }
        public String filterID { get; set; }
    }
}
