﻿using System;

namespace JiraAssist.Models.ViewModels
{
    public class RegistrationViewModel : AuthBaseViewModel
    {
        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Role { get; set; }

        private RegistrationViewModel() { }

        public RegistrationViewModel(String log, String pass, String rol, String firstName, String lastName)
            : base(log, pass)
        {
            this.Role = rol;
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}
