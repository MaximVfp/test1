﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace JiraAssist.Models.ViewModels
{
    public class AuthBaseViewModel
    {
        public String Login { get; set; }

        public String Password { get; set; }

        public AuthBaseViewModel() { }

        public AuthBaseViewModel(String log, String pass)
        {
            this.Login = log;
            this.Password = pass;
        }
    }
}
