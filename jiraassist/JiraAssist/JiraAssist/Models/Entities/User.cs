﻿using System;
using Microsoft.AspNetCore.Identity;


namespace JiraAssist.Models.Entities
{
    public class User : IdentityUser
    {
        public String FirstName { get; set; }

        public String LastName { get; set; }

        public String Login { get; set; }

        public String Password { get; set; }

        public String Role { get; set; }

        private User() { }

        public User(String log, String pass, String rol, String firstName, String lastName)
        {
            this.Login = log;
            this.Password = pass;
            this.Role = rol;
            this.FirstName = firstName;
            this.LastName = lastName;
        }
    }
}
