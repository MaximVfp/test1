﻿using BOL;
using System;
using JiraLoader;
using System.Linq;
using Newtonsoft.Json;
using JiraAssist.Helpers;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using JiraAssist.Models.Entities;
using System.Collections.Generic;
using JiraAssist.Models.ViewModels;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using JiraAssist.DBFileSystem;
using System.Runtime.Serialization.Json;
using JiraAssist.Models.ViewModels.Response;
using Reporter;
using System.Net.Http;
using System.IO;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Cors;
using MailKit;
using MimeKit;
using MailKit.Net.Smtp;

namespace JiraAssist.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        private IDBFileSystem _ConnDBFileSystem;

        private IJiraConnector _JiraConnector;

        private IConfiguration _Configuration;

        public AccountController(IDBFileSystem _ConnFileSystem, IJiraConnector _JiraConnector)
        {
            this._ConnDBFileSystem = _ConnFileSystem;

            this._JiraConnector = _JiraConnector;

            var pth = Environment.CurrentDirectory;

            this._Configuration = new ConfigurationBuilder()                
                .AddJsonFile(@"C:\Users\DmitryJ\Desktop\prj\jiraassist\jiraassist\JiraAssist\JiraAssist\conf.json").Build();
           
        }


        [HttpPost("[action]")]
        public String Login([FromBody] LoginViewModel loginInfo)
        {
            if (!_ConnDBFileSystem.CheckUserExistence(loginInfo.Login, loginInfo.Password))
                return AuthOptions.AUTH_ERROR;

            return BuildResponse(_ConnDBFileSystem.GetUser(loginInfo.Login) as User);
        }

        [HttpPost("[action]")]
        public String Registration([FromBody] RegistrationViewModel registrationInfo)
        {
            if (_ConnDBFileSystem.CheckUserExistence(registrationInfo.Login))
                return AuthOptions.AUTH_ERROR;

            var user = new User(registrationInfo.Login,
                            registrationInfo.Password,
                            "user",
                            registrationInfo.FirstName,
                            registrationInfo.LastName);

            _ConnDBFileSystem.AddUser(user);

            return BuildResponse(user);
        }

        [HttpPost("[action]")]
        public String GetDataTable([FromBody] GetDataTableViewModel data)
        {
            if (!_ConnDBFileSystem.CheckTokenExistence(data.token))
                return AuthOptions.AUTH_ERROR;

            (String log, String pass) LP = _ConnDBFileSystem.GetLP(data.token);

            //later
            var response = _JiraConnector.RunRestQuery($"{LP.log}", $"{LP.pass}", "search?jql=report");

            var ser = new DataContractJsonSerializer(typeof(JiraIssuesSet));
            var listIss = (ser.ReadObject(response) as JiraIssuesSet).issues;

            HttpContext.Session.Set(data.token, listIss);

            var table = new List<DataTableResponseViewModel>(listIss.Count);

            foreach (var i in listIss)
                table.Add(new DataTableResponseViewModel()
                {
                    key = i.pKey,
                    assignee = i.Assignee.FullName,
                    deadLine = i.DeadLine,
                    summary = i.Summary,
                    duedate = i.DueDate
                });

            return BuildResponse(data.token, table);
        }

        [HttpPost("[action]")]
        //[EnableCors(origins: "*", headers: "*", methods: "*")]
        public FileResult GetExcel([FromBody] GetExcelViewModel data)
        {
            if (HttpContext.Session.Keys.Contains(data.token))
                return File(
                    DailyReport.BuildReport(HttpContext.Session.Get<List<Issue>>(data.token)).GetAsByteArray(),
                    "application/xlsx",
                    "table"
                    );
            else
                return null;
        }


        private String BuildResponse(User user)
        {
            var token = BuildToken(user);

            _ConnDBFileSystem.AddTokenToUser(user, token);

            return JsonConvert.SerializeObject(new { access_token = token });
        }

        private String BuildResponse(String token, Object data)
        {
            return JsonConvert.SerializeObject(new { access_token = token, message = data });
        }

        private String BuildToken(User user)
        {
            var identity = new ClaimsIdentity(
                new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                    new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role)
                },
                "Token",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType
                );

            var dataTime = DateTime.UtcNow;
            var jwt = new JwtSecurityToken(
                    issuer: AuthOptions.ISSUER,
                    audience: AuthOptions.AUDIENCE,
                    notBefore: dataTime,
                    claims: identity.Claims,
                    expires: dataTime.Add(TimeSpan.FromMinutes(AuthOptions.LIFETIME)),
                    signingCredentials: new SigningCredentials(AuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256));

            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }


        private string CreateHTML(List<DataTableResponseViewModel> users)
        {
            var dateMas = new List<String>(users.Count);

            var name = " ";
            for (int i = 0; i < users.Count; i++)            
                if (users[i].assignee != name)
                    name = users[i].assignee;
                else
                    users[i].assignee = " ";            

            foreach (var user in users)
                dateMas.Add(user.deadLine == null ?
                    " " :
                   ((DateTime)user.deadLine).ToString("MM/dd/yyyy"));

            var header = "";
            var body = "";

            header += "<h1>НЕ ЗАБЫТЬ ИСПРАВИТЬ Хардкод!!!</h1> <br>";

            header += "<b>From:</b> "+ _Configuration["from"] + " &lt; <a  href='"+ _Configuration["fromMail"] + "' >"+ _Configuration["fromMail"] + "</a> > <br>";
            header += "<b>Sent:</b>" + DateTime.Now + "<br>";
            header += "<b>To:</b> "+ _Configuration["to"] + " &lt; <a  href='"+ _Configuration["toMail"] + "' >"+ _Configuration["toMail"] + "</a> > <br>";
            header += "<b>Subject:</b> "+ _Configuration["subject"] + DateTime.Now.ToString("MM/dd/yyyy") + "<br> <br><br>";
            header += "Hello "+ _Configuration["to"].Split(' ')[0] + ",  <br><br><br>";
            header += "Daily status for "+ DateTime.Now.ToString("MM/dd/yyyy") + "  <br>";
            body += "<table style = 'height:15pt'><tbody><tr><td style = 'width:351px;align:left'><b>Team member</b></td><td style = 'width:1308px;align:left'><b>Work</b></td><td style = 'width:155px;align:center'><b>Plan to finish</b></td><td style = 'width:155px;align:center'><b>DeadLine</b></td></tr>";

            foreach (var user in users)
            {
                if (user.assignee != " ")
                    body += "<tr style = ' border-top:1px solid black;border-bottom:1px solid black;height:15pt'><td></td><td></td><td></td><td></td></tr>";

                body += "<tr style = 'height:15pt'><td style = 'width:351px;align:left'>" + user.assignee + "</td><td style = 'width:1308px;align:left'>" + user.key + "-" + user.summary + "</td>";


                var duedateFlag = user.duedate < (DateTime.Now).Date;
                var deadLineFlag = user.deadLine < (DateTime.Now).Date;

                body += "<td style = 'width:155px;align:center";

                if (duedateFlag) body += ";background: red;";
                body += "'>" + ((DateTime)user.duedate).ToString("MM/dd/yyyy") + "</td>";

                body += "<td style = 'width:155px";

                if (deadLineFlag) body += ";background: red;'>";
                else body += "' class = 'check'>";

                body += (user.deadLine == null ?
                    " " :
                     ((DateTime)user.deadLine).ToString("MM/dd/yyyy")) +
                    "</td></tr>";

            }

            body += "<tr style = 'border-top: 1px solid black'><td></td><td></td><td></td><td></td></tr>";
            body += "</table> ";

            return header + body;
        }

        //TOKENS
        [HttpPost("[action]")]
        public String SendPreview([FromBody] List<DataTableResponseViewModel> arrUsers)
        {
            return JsonConvert.SerializeObject(CreateHTML(arrUsers));
        }

        //TOKENS
        [HttpPost("[action]")]
        public string SendMessage([FromBody] List<DataTableResponseViewModel> arrUsers)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(_Configuration["from"], _Configuration["fromMail"]));
            message.To.Add(new MailboxAddress(_Configuration["to"], _Configuration["toMail"]));
            message.Subject = $"{_Configuration["subject"]}: " + DateTime.Now;
            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = CreateHTML(arrUsers);
            message.Body = bodyBuilder.ToMessageBody();
            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587, false);
                client.Authenticate(_Configuration["fromMail"], _Configuration["fromPassword"]);

                client.Send(message);

                client.Disconnect(true);
            }
            return JsonConvert.SerializeObject(CreateHTML(arrUsers));
        }

        [HttpGet("[action]")]
        public string QWE() => "qwe hello";
    }
}
