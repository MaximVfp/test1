﻿using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;

namespace JiraAssist.Helpers
{
    public static class AuthOptions
    {
        public static String ISSUER { get; private set; } = "tokin-publisher";

        public static String AUDIENCE { get; private set; } = "tokin-consumer";

        public static Int32 LIFETIME { get; private set; } = 1;

        public static String AUTH_ERROR = "{ access_token: null }";

        private static String KEY { get; set; } = "3141592653589793238462643";

        public static SymmetricSecurityKey GetSymmetricSecurityKey() =>
                  new SymmetricSecurityKey(System.Text.Encoding.ASCII.GetBytes(KEY));
    }
}
