﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using JiraLoader;
using Microsoft.Extensions.DependencyInjection;

namespace JiraAssist.Fake
{
    public static class DiskedData
    {
        public static void Init()
        {
            if (Jira.Services.Count == 0)
                Jira.Services.AddTransient<IJiraConnector, FakeJiraConnector>();
        }


        static string path = @"..\..\..\jiraassist\JiraAssist\Tests\Testcases\";
        public static string GetJsonByKey(string pKey)
        {
            //TO-DO: need to add a cache
            string text = System.IO.File.ReadAllText($"{path}{pKey}.JSON");
            return text;
        }

        public static Stream ToStream(this String s)
        {
            var stream = new MemoryStream();
            var writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

    }
}
