﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using BOL;
using Microsoft.Extensions.DependencyInjection;

namespace DAL
{
    public class DBConnector
    {
        IDB db;
        public static ServiceCollection Services = new ServiceCollection();

        public DBConnector()
        {
            db = Services.BuildServiceProvider().GetService<IDB>();
        }
        public void SaveIssue(Issue issue)
        {
            var ser = new DataContractJsonSerializer(typeof(Issue));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, issue);
            var jsonData =  Encoding.ASCII.GetString(stream.ToArray());


            var spParams = new Dictionary<string, object>(5);
            spParams.Add("@ID", issue.ID);
            spParams.Add("@pKey", issue.pKey);
            spParams.Add("@ProjectID", issue.Project.ID);
            spParams.Add("@Updated", issue.Updated);
            spParams.Add("@JsonData", jsonData);

            db.ExecuteSP("[JIRA].[SaveIssue]", spParams);
        }


    }
}
