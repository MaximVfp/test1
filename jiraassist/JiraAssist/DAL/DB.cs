﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DB: IDB
    {
        protected SqlConnection GetConnection()
        {
#if DEBUG
            return new SqlConnection("Data Source=main;Initial Catalog=jiraassist;Integrated Security=True");
#else
            return new SqlConnection("Data Source=main;Initial Catalog=jiraassist;Integrated Security=True");
#endif

        }

        public bool ExecuteSql(string sql, Dictionary<string, object> spParams, System.Data.CommandType commandType = System.Data.CommandType.Text)
        {
            using (SqlConnection cnn = GetConnection())
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, cnn))
                {
                    cmd.CommandType = commandType;
                    foreach (var par in spParams)
                    {
                        var val = par.Value;
                        if (val == null)
                            val = DBNull.Value;
                        cmd.Parameters.AddWithValue(par.Key, val);
                    }
                    cmd.ExecuteNonQuery();

                    cnn.Close();
                    return true;
                }
            }
        }

        //public object ExecuteSqlData(string sql, Dictionary<string, object> spParams, System.Data.CommandType commandType = System.Data.CommandType.Text)
        //{
        //    using (SqlConnection cnn = GetConnection())
        //    {
        //        cnn.Open();
        //        using (SqlCommand cmd = new SqlCommand(sql, cnn))
        //        {
        //            cmd.CommandType = commandType;
        //            foreach (var par in spParams)
        //            {
        //                var val = par.Value;
        //                if (val == null)
        //                    val = DBNull.Value;
        //                cmd.Parameters.AddWithValue(par.Key, val);
        //            }
        //            var result = cmd.ExecuteScalar();

        //            cnn.Close();
        //            return result;
        //        }
        //    }
        //}


        public bool ExecuteSP(string spName, Dictionary<string, object> spParams)
        {
            return ExecuteSql(spName, spParams, System.Data.CommandType.StoredProcedure);
        }


        public DataTable ReadData(string sql, Dictionary<string, object> spParams = null)
        {
            using (SqlConnection cnn = GetConnection())
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, cnn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (spParams != null)
                    {
                        foreach (var par in spParams)
                        {
                            cmd.Parameters.AddWithValue(par.Key, par.Value);
                        }
                    }
                    var dataReader = cmd.ExecuteReader();
                    var dataTable = new DataTable();
                    dataTable.Load(dataReader);

                    cnn.Close();
                    return dataTable;
                }
            }
        }

        public object ReadScalar(string sql, Dictionary<string, object> spParams = null)
        {
            object result = null;
            using (SqlConnection cnn = GetConnection())
            {
                cnn.Open();
                using (SqlCommand cmd = new SqlCommand(sql, cnn))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    if (spParams != null)
                    {
                        foreach (var par in spParams)
                        {
                            cmd.Parameters.AddWithValue(par.Key, par.Value);
                        }
                    }
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            result = reader[0];
                        }
                        reader.Close();
                    }

                    cnn.Close();
                    return result;
                }
            }
        }


    }
}
