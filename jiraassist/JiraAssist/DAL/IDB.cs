﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
    public interface IDB
    {
        bool ExecuteSP(string spName, Dictionary<string, object> spParams);
    }
}
