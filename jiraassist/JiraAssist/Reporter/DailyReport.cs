﻿using BOL;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Reporter.Utils;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Reporter
{
    public class DailyReport
    {

        public static ExcelPackage BuildReport(IEnumerable<object> items)
        {
            int maxColumns = 4;
            var package = new ExcelPackage();
            var itemsToExport = (items as List<Issue>);

            ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("Report");
            worksheet.InsertRow(1, itemsToExport.Count + 1);
            worksheet.InsertColumn(1, maxColumns);

            ReportHeader.AddHeader(worksheet, new ReportHeader("Team Member", 50), new ReportHeader("Work", 85), new ReportHeader("When we plan to finish", 22), new ReportHeader("DeadLine", 22));

            int rowIndex = 1;
            int prevAuthorEndRow = 2;
            string previousName = "";
            foreach (Issue iss in itemsToExport)
            {
                if (iss == null) continue;
                string currName = iss.Assignee?.FullName;
                if (currName != previousName)
                {
                    if (rowIndex != 1)
                    {
                        worksheet.InsertRow(rowIndex + 1, 1);
                        worksheet.Cells[prevAuthorEndRow, 1, rowIndex, maxColumns].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);
                        rowIndex++;
                        prevAuthorEndRow = rowIndex + 1;

                    }
                    worksheet.Cells[rowIndex + 1, 1].Value = currName;
                    previousName = currName;

                }

                worksheet.Cells[rowIndex + 1, 2].Value = $"{iss.pKey} - {iss.Summary}";

                var cell = worksheet.Cells[rowIndex + 1, 3];
                DueDateColoring(cell, iss.DueDate);

                cell = worksheet.Cells[rowIndex + 1, 4];
                DueDateColoring(cell, iss.DeadLine);

                rowIndex++;
            }

            worksheet.Cells[prevAuthorEndRow, 1, rowIndex, maxColumns].Style.Border.BorderAround(ExcelBorderStyle.Medium, Color.Black);

            return package;

        }

        private static void DueDateColoring(ExcelRange cell, DateTime? date)
        {
            if (!date.HasValue) return;
            DateTime celldate = (date.Value);
            cell.Value = celldate.ToShortDateString();
            if (celldate < DateTime.Now.Date)
            {
                //worksheet.Cells[rowIndex + 1, 3].StyleName = "OutOfDatedStyle";
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(Color.Red);
            }
            else if (celldate == DateTime.Now.Date)
            {
                cell.Style.Font.Bold = true;
            }
        }
    }
}


