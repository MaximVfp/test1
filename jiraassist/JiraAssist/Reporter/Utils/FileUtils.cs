﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Reporter.Utils
{
    public class FileUtils
    {
        static DirectoryInfo _outputDir = null;
        public static DirectoryInfo OutputDir
        {
            get
            {
                return _outputDir;
            }
            set
            {
                _outputDir = value;
                if (!_outputDir.Exists)
                {
                    _outputDir.Create();
                }
            }
        }
        public static FileInfo GetFileInfo(string fileName, bool deleteIfExists = true, DirectoryInfo altOutputDir = null)
        {
            if (altOutputDir != null)
            {
                fileName = altOutputDir.FullName + Path.DirectorySeparatorChar + fileName;
            }
            FileInfo fi = null;
            try
            {
                fi = new FileInfo(fileName);
                if (deleteIfExists && fi.Exists)
                {
                    fi.Delete();  // ensures we create a new workbook
                }
            }
            catch (Exception ex)
            {
                fi = null;
            }
            return fi;
        }

        internal static DirectoryInfo GetDirectoryInfo(string directory)
        {
            var di = new DirectoryInfo(_outputDir.FullName + Path.DirectorySeparatorChar + directory);
            if (!di.Exists)
            {
                di.Create();
            }
            return di;
        }
    }
}
