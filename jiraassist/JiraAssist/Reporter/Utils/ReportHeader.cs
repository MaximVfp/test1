﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Reporter.Utils
{
    class ReportHeader
    {
        public ReportHeader(string caption, float width)
        {
            this.Caption = caption;
            this.Width = width;
        }
        string Caption { get; set; }
        double Width { get; set; }

        public static int AddHeader(ExcelWorksheet sheet, params ReportHeader[] itms)
        {
            for (int i = 0; i < itms.Count(); i++)
            {

                var cell = sheet.Cells[1, i + 1];
                cell.Value = itms[i].Caption;
                sheet.Column(i + 1).Width = itms[i].Width;
                cell.Style.Font.Bold = true;
                cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
                cell.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.LightGray);
            }
            return itms.Count();
        }
    }
}

