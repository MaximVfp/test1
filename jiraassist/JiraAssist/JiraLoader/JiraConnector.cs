﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace JiraLoader
{
    public class JiraConnector: IJiraConnector
    {
        private const string JIRA_PATH = "https://applicint-usa.atlassian.net/rest/api/2";

        private string GetEncodedCredentials(string username, string password)
        {
            string mergedCredentials = string.Format("{0}:{1}", username, password);
            byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(mergedCredentials);
            return Convert.ToBase64String(byteCredentials);
        }

        public Stream RunRestQuery(string userName, string password, string query)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", GetEncodedCredentials(userName, password));

                //Putting content-type into the Header.
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //i am using StringContent because i am creating console application, no any serialize i used for maniputlate the string.

                var sTask = client.GetStreamAsync($"{JIRA_PATH}/{query}");
                return sTask.Result;
            }
            catch (Exception ex)
            {
                //To-Do: Add log $"[Jira Error]:There is a problem getting data from Jira: {query} {Environment.NewLine} {ex.Message}".ToStream();
                return null;
            }

        }
    }
}
