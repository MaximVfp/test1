﻿using System.IO;

namespace JiraLoader
{
    public interface IJiraConnector
    {
        Stream RunRestQuery(string userName, string password, string query);
    }
}