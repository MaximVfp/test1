﻿using BOL;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;

namespace JiraLoader
{
    public class Jira: IJira
    {
        public string UserName { get; private set; }
        public string Password { get; private set; }

        private bool GetIssuesForPage(string jql, int pageIndex, List<Issue> resultList)
        {
            var result = false;
            List<Issue> fromRest;
            var query = $"search?jql={jql}&startAt={50 * (pageIndex - 1)}";
            Stream rest = jConn.RunRestQuery(UserName, Password, query);
            if (rest == null)
            {
                fromRest = new List<Issue>(0);
                result = false;
            }
            else
            {
                var ser = new DataContractJsonSerializer(typeof(JiraIssuesSet));
                var obj = ser.ReadObject(rest) as JiraIssuesSet;
                fromRest = obj.issues;
                result = (obj.total > pageIndex * 50);
            }

            foreach (var iss in fromRest)
            {
                resultList.Add(iss);
            }

            return result;
        }

        public IEnumerable<Issue> GetIssuesByJQL(string jql)
        {
            var result = new List<Issue>(50);
            int pageIndex = 1;
            while (GetIssuesForPage(jql, pageIndex, result))
            {
                pageIndex++;
            }

            foreach(Issue iss in result)
                iss.Project = GetProjectByKey(iss.ProjectKey);

            return result;
        }

        public Issue GetIssueByKey(string key)
        {
            Issue result = null;
            var rest = jConn.RunRestQuery(UserName, Password, $"issue/{key}");

            if (rest != null)
            {
                var ser = new DataContractJsonSerializer(typeof(Issue));
                result = ser.ReadObject(rest) as Issue;
            }
            result.Project = GetProjectByKey(result.ProjectKey);
            return result;
        }

        private List<Project> _ProjectCache = new List<Project>();
        public List<Project> AllProjects
        {
            get
            {
                if (_ProjectCache.Count == 0)
                    _ProjectCache = LoadAllProjects();
                return _ProjectCache;
            }
        }

        public Project GetProjectByKey(string pkey)
        {
            var result = _ProjectCache.SingleOrDefault(pr => pr.pKey == pkey);
            if (result != null) return result;
            result = new Project();
            string query = $"project/{pkey}";
            var rest = jConn.RunRestQuery(UserName, Password, query);
            if (rest != null)
            {

                var ser = new DataContractJsonSerializer(typeof(Project));
                result = ser.ReadObject(rest) as Project;
                if (result != null)
                    _ProjectCache.Add(result);
            }
            return result;
        }


        private List<Project> LoadAllProjects()
        {
            var result = new List<Project>(50);
            var rest = jConn.RunRestQuery(UserName, Password, "project");

            if (rest != null)
            {
                var ser = new DataContractJsonSerializer(typeof(List<Project>));
                var projectIDs = ser.ReadObject(rest) as List<Project>;


                foreach (var id in projectIDs)
                {
                    result.Add(GetProjectByKey(id.pKey));
                }
            }

            return result;
        }

        public IEnumerable<Worklog> GetWorklogsForIssue(int issueId)
        {
            List<Worklog> result = new List<Worklog>(3);
            var rest = jConn.RunRestQuery(UserName, Password, $"issue/{issueId}/worklog");

            if (rest != null)
            {
                var ser = new DataContractJsonSerializer(typeof(WorklogSet));
                result = (ser.ReadObject(rest) as WorklogSet).worklogs;
            }
            return result;
        }



        public IEnumerable<Issue> GetLatest5minutesIssues()
        {
            return GetIssuesByJQL($"updated > -50m").ToList();
        }

        IJiraConnector jConn;
        public static ServiceCollection Services = new ServiceCollection();

        public Jira(string userName, string userPass) 
        {
            UserName = userName;
            Password = userPass;
            jConn = Services.BuildServiceProvider().GetService<IJiraConnector>();
        }
    }
}
