using BOL;
using JiraLoader;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{
    public class RestTest
    {
        [Fact]
        public void FakeJiraTest()
        {
            FakeJiraConnector cnn = new FakeJiraConnector();
            Assert.Equal("search/LIST.TRAN-1",  cnn.GetFileNameFromQuery("search?jql=LIST.TRAN-1&startAt=0"));
            Assert.Equal("search/LIST.TRAN-1", cnn.GetFileNameFromQuery("search?jql=LIST.TRAN-1&startAt=50"));
            Assert.Equal("issue/PRU-1506", cnn.GetFileNameFromQuery("issue/PRU-1506"));
            Assert.Equal("issue/10353/worklog", cnn.GetFileNameFromQuery("issue/10353/worklog"));


        }

        
        [Fact]
        public void Test1()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");

            List<Issue> issues = jira.GetIssuesByJQL("LIST.TRAN-1").ToList();
            Issue iss = issues[0];
            Assert.Equal("TRAN-1", iss.pKey);
            Assert.Equal("TRAN", iss.Project.pKey);
            Assert.Equal(10010, iss.IssueType.ID);
            Assert.Equal("mferoahjr", iss.Assignee.Name);
            Assert.False(iss.Assignee.IsDevTeam);
            Assert.Equal(0, iss.DueDate.Ticks);
            Assert.Equal("Transamerica estimate for new forms on Exam Complete", iss.Summary);
            Assert.Equal("Transamerica", iss.Project.Name.Trim());
            Assert.Equal(new DateTime(2018, 03, 21), iss.Updated.Date);
        }

        [Fact]
        public void TestCOMPandVER()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");
                        
            Issue iss = jira.GetIssueByKey("PRU-1506");

            Assert.Equal("PRU-1506", iss.pKey);
            Assert.Equal("PRU", iss.Project.pKey);
            Assert.Equal(10004, iss.IssueType.ID);
            Assert.Equal("jthomas", iss.Assignee.Name);
            Assert.Equal(Convert.ToDateTime("2018-09-04"), iss.DueDate);
            Assert.Single(iss.Components);
            Assert.Equal("CallComplete Implementation", iss.Components[0].Name);
            Assert.Equal(10000, iss.Components[0].ID);
            Assert.Equal("The initial implementation of CallComplete", iss.Components[0].Description);
            Assert.Single(iss.FixVersions);
            Assert.Equal(10172, iss.FixVersions[0].ID);
            Assert.Equal("2.6.45", iss.FixVersions[0].Name);

            Assert.Equal(10033, iss.IssueStatus.ID);
            Assert.Equal("Customer contacted--waiting for response", iss.IssueStatus.Name);
        }

        [Fact]
        public void TestIssue2()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");

            List<Issue> issues = jira.GetIssuesByJQL("LIST.PAC-20").ToList();

            Issue iss = issues[0];

            Assert.Equal("PAC-20", iss.pKey);
            Assert.Equal("PAC", iss.Project.pKey);
            Assert.Equal(10002, iss.IssueType.ID);
            Assert.Equal("App Profile - Cover Page layout", iss.Summary);
            Assert.Equal("Pacific Life", iss.Project.Name.Trim());
            Assert.Equal(new DateTime(2018, 03, 27), iss.DueDate.Date);

            Assert.Equal(2, iss.FixVersions.Count);
            var fixVer = iss.FixVersions[0];
            Assert.Equal(10005, fixVer.ID);
            Assert.Equal("APPS CC: PAC-20. App Profile - Cover Page layout, PAC-22. New NH HIV Form", fixVer.Description);
            Assert.Equal("2.1.7.4", fixVer.Name);
            Assert.False(fixVer.Archived);
            Assert.Equal("2018-04-04", fixVer.ReleaseDate);

            fixVer = iss.FixVersions[1];
            Assert.Equal(10006, fixVer.ID);
            Assert.Equal("APPS EC and E1 EC - PAC-22. New NH HIV Form", fixVer.Description);
            Assert.Equal("2.1.6.8", fixVer.Name);
            Assert.False(fixVer.Archived);
            Assert.Equal("2018-04-04", fixVer.ReleaseDate);

            Assert.Single(iss.Labels);
            Assert.Equal("CoverPage", iss.Labels[0]);
        }

        [Fact]
        public void TestDeadLine()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");

            Issue iss = jira.GetIssueByKey("PAC-123");
            Assert.Equal(new DateTime(2018, 09, 20), iss.DeadLine);

            iss = jira.GetIssueByKey("PRU-1587");
            Assert.Null(iss.DeadLine);
        }

        [Fact]
        public void TestDueDateConversions()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");

            Issue iss = jira.GetIssueByKey("PAC-123");
            Assert.Equal(new DateTime(2018, 09, 20), iss.DueDate);

            iss.SetDueDate(new DateTime(2018, 09, 20));
            Assert.Equal(new DateTime(2018, 09, 20), iss.DueDate);

            iss.SetDueDate(new DateTime(2018, 12, 01, 5, 30, 29));
            Assert.Equal(new DateTime(2018, 12, 01), iss.DueDate);



        }


        private void TestTranProject(Project tran)
        {
            Assert.Equal(6, tran.Versions.Count);
            var ver2test = tran.Versions.SingleOrDefault(ver => ver.ID.Equals(10032));
            Assert.Equal("Examone: patch (remove iFrames + prevent multi clicking)", ver2test.Description);
            Assert.Equal("2.6.9.4", ver2test.Name);
            Assert.False(ver2test.Archived);
            Assert.True(ver2test.Released);
            Assert.Equal("Examone: patch (remove iFrames + prevent multi clicking)", ver2test.Description);
        }

        [Fact]
        public void TestProjects()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");

          
            var tranProj = jira.GetProjectByKey("TRAN");
            TestTranProject(tranProj);
            var projects = jira.AllProjects;
            TestTranProject(projects.SingleOrDefault(pr => pr.pKey.Equals("TRAN")));
        }



        [Fact]
        public void TestWorklog()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");

            List<Worklog> wlList = jira.GetWorklogsForIssue(10353).ToList();
            Worklog wl = wlList[0];

            Assert.Equal(10353, wl.ID);
            Assert.Equal("kirillb", wl.Author.Name);
            Assert.Equal("kirillb", wl.Author.ID);
            Assert.True(wl.Author.IsDevTeam);
            Assert.Equal("Kirill Bodyako", wl.Author.FullName);
            Assert.Equal("1h", wl.TimeSpent);
            Assert.Equal(3600, wl.TimeSpentSeconds);
            Assert.Equal(new DateTime(2018, 03, 23, 18, 31, 0), wl.WorkDate);
            Assert.Equal(10603, wl.IssueID);

        }


    }
}
