﻿using BOL;
using JiraLoader;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using Reporter;
using Reporter.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Tests
{

    public class DailyReportTest
    {

        private Issue AddIssue(string authorName, string pKey, string summary, DateTime dueDate)
        {
            Issue result = new Issue()
            {
                pKey = pKey,
                Summary = summary
            };

            result.SetDueDate(dueDate);
            result.SetAssignee(new Person()
            {
                FullName = authorName,
                Name = authorName
            });
            return result;
        }

        private bool CheckCellValue(ExcelWorksheet sheet, int row, int col, string expectingValue)
        {
            var val = sheet.Cells[row, col].Value ?? "";
            return (val.ToString() == expectingValue);
        }

        private bool CheckCellBorder(ExcelWorksheet sheet, int row, int col, bool left, bool top, bool right, bool bottom)
        {

            var border = sheet.Cells[row, col].Style.Border;

            if ((border.Left.Style != OfficeOpenXml.Style.ExcelBorderStyle.Medium) && (left))
                return false;

            if ((border.Right.Style != OfficeOpenXml.Style.ExcelBorderStyle.Medium) && (right))
                return false;

            if ((border.Top.Style != OfficeOpenXml.Style.ExcelBorderStyle.Medium) && (top))
                return false;

            if ((border.Bottom.Style != OfficeOpenXml.Style.ExcelBorderStyle.Medium) && (bottom))
                return false;
            return true;
        }

        private bool CheckCell(ExcelWorksheet sheet, int row, int col, string expectingValue, bool left = false, bool top = false, bool right = false, bool bottom = false)
        {
            if (!CheckCellValue(sheet, row, col, expectingValue)) return false;
            return CheckCellBorder(sheet, row, col, left, top, right, bottom);

        }

        private void CheckCellFill(ExcelWorksheet sheet, int row, int col, string rgb, ExcelFillStyle fillStyle = ExcelFillStyle.None)
        {
            if (fillStyle != ExcelFillStyle.None)
            {
                Assert.Equal(rgb, sheet.Cells[row, col].Style.Fill.BackgroundColor.Rgb);
            }

            Assert.Equal(fillStyle, sheet.Cells[row, col].Style.Fill.PatternType);

        }



        [Fact]
        public void TestReport()
        {
            DiskedData.Init();
            var jira = new Jira("n/a", "n/a");

            List<Issue> issues = new List<Issue>(5);
            issues.Add(jira.GetIssueByKey("EX-73"));
            issues.Add(jira.GetIssueByKey("EX-46"));

            issues.Add(jira.GetIssueByKey("EX-85"));
            issues.Add(jira.GetIssueByKey("EX-49"));
            issues.Add(jira.GetIssueByKey("SAM-12"));

            /*items.Add(AddIssue("Ai Tran Ngoc Nhan", "EX-73", "\"Communication Logs Report\" page: do not store a lot of requests", new DateTime(2018, 4, 11)));
            items.Add(AddIssue("Ai Tran Ngoc Nhan", "EX-46", "ExpressComplete installation and configuration for local work", new DateTime(2018, 4, 11)));

            items.Add(AddIssue("Alexander Ivanchikov", "EX-85", "\"External Interfaces\" specification changes", new DateTime(2018, 4, 11)));
            items.Add(AddIssue("Alexander Ivanchikov", "EX-49", "Need Smoker Codes in Underwriter Class API", new DateTime(2018, 4, 25)));
            items.Add(AddIssue("Alexander Ivanchikov", "SAM-12", "SAML 2.0 SSO for Sammons", new DateTime(2018, 4, 30)));
            */
            using (var package = DailyReport.BuildReport(issues))
            {
                package.SaveAs(FileUtils.GetFileInfo(@"c:/temp/test.xlsx"));
                var sheet = package.Workbook.Worksheets["Report"];

                for (int col = 1; col <= 4; col++)
                {
                    Assert.Equal(ExcelFillStyle.Solid, sheet.Cells[1, col].Style.Fill.PatternType);
                    Assert.Equal("FFD3D3D3", sheet.Cells[1, col].Style.Fill.BackgroundColor.Rgb);

                }
                int author_column = 1;
                int issue_column = 2;
                int due_column = 3;
                int dead_column = 4;
                int row2check = 2;
               
                Assert.True(CheckCell(sheet, row2check, author_column, "Ai Tran Ngoc Nhan", left: true, top: true));
                Assert.True(CheckCell(sheet, row2check, issue_column, "EX-73 - \"Communication Logs Report\" page: do not store a lot of requests", top: true));
                Assert.True(CheckCell(sheet, row2check, due_column, "7/4/2018", top: true, right: false));
                Assert.True(CheckCell(sheet, row2check, dead_column, "7/21/2018", top: true, right: true));
                CheckCellFill(sheet, row2check, due_column, "FFFF0000", ExcelFillStyle.Solid);
                CheckCellFill(sheet, row2check, dead_column, "FFFF0000", ExcelFillStyle.Solid);

                row2check = 3;
                Assert.True(CheckCell(sheet, row2check, author_column, "", left: true, bottom: true));
                Assert.True(CheckCell(sheet, row2check, issue_column, "EX-46 - ExpressComplete installation and configuration for local work", bottom: true));
                Assert.True(CheckCell(sheet, row2check, due_column, "4/13/2018", right: false, bottom: true));
                Assert.True(CheckCell(sheet, row2check, dead_column, "", right: true, bottom: true));
                CheckCellFill(sheet, row2check, due_column, "FFFF0000", ExcelFillStyle.Solid);
                CheckCellFill(sheet, row2check, dead_column, "n/a", ExcelFillStyle.None);


                //separate authors by space line
                row2check =4;
                Assert.True(CheckCell(sheet, row2check, author_column, ""));
                Assert.True(CheckCell(sheet, row2check, issue_column, ""));
                Assert.True(CheckCell(sheet, row2check, due_column, ""));
                CheckCellFill(sheet, row2check, due_column, "n/a", ExcelFillStyle.None);

                row2check =5;
                Assert.True(CheckCell(sheet, row2check, author_column, "Alexander Ivanchikov", left: true, top: true));
                Assert.True(CheckCell(sheet, row2check, issue_column, "EX-85 - \"External Interfaces\" specification changes", top: true));
                Assert.True(CheckCell(sheet, row2check, due_column, "4/13/2018", top: true, right: false));
                Assert.True(CheckCell(sheet, row2check, dead_column, "", top: true, right: true));
                CheckCellFill(sheet, row2check, due_column, "FFFF0000", ExcelFillStyle.Solid);

                row2check =6;
                Assert.True(CheckCell(sheet, row2check, author_column, "", left: true));
                Assert.True(CheckCell(sheet, row2check, issue_column, "EX-49 - Need Smoker Codes in Underwriter Class API"));
                Assert.True(CheckCell(sheet, row2check, due_column, "6/20/2099"));
                Assert.True(CheckCell(sheet, row2check, dead_column, "", right: true));
                CheckCellFill(sheet, row2check, due_column, "n/a", ExcelFillStyle.None);
                

                row2check = 7;
                Assert.True(CheckCell(sheet, row2check, author_column, "", left: true, bottom: true));
                Assert.True(CheckCell(sheet, row2check, issue_column, "SAM-12 - SAML 2.0 SSO for Sammons", bottom:true));
                Assert.True(CheckCell(sheet, row2check, due_column, "4/30/2018", bottom:true));
                Assert.True(CheckCell(sheet, row2check, dead_column, "", right: true, bottom:true));
                CheckCellFill(sheet, row2check, due_column, "FFFF0000", ExcelFillStyle.Solid);

            }

        }
    }
}
