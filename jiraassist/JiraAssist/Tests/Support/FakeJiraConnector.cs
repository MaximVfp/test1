﻿using JiraLoader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Tests
{
    public class FakeJiraConnector : IJiraConnector
    {
        internal string GetFileNameFromQuery(string query)
        {
            //"search?jql=LIST.TRAN-1&startAt=0"
            string result = query.Replace("search?jql=", "search/");
            int idx = result.IndexOf("&");
            if (idx > 0)
            {
                result = result.Remove(idx);
            }
            return result;
        }
        public Stream RunRestQuery(string userName, string password, string query)
        {
            return DiskedData.GetJsonByKey(GetFileNameFromQuery(query)).ToStream();
        }
    }
}

